<?php
require_once(__DIR__.'/../src/Lemon.class.php');
require_once(__DIR__.'/../src/Tomato.class.php');
require_once(__DIR__.'/../src/GreenGrape.class.php');
require_once(__DIR__.'/../src/Product.class.php');
require_once(__DIR__.'/../src/Cart.class.php');


class CartTest extends PHPUnit_Framework_TestCase {


    public function testGetPriceOf1Lemon()
    {
        $cart = new Cart();

        // Create a stub for the Lemon class.
        $lemon = $this->getMockBuilder('Product')
                      ->setMethods(array('getType','setPrices','getPrice'))
                      ->disableOriginalConstructor()
                      ->getMock();

        // Configure the stub.
        $lemon->expects($this->any())->method('getType')->will($this->returnValue('lemon'));
        $lemon->expects($this->any())->method('setPrices')->will($this->returnValue([0 => 0.50, 11 => 0.45]));
        $lemon->expects($this->once())->method('getPrice')->will($this->returnValue(0.50));
        $lemon->__construct('lemon');

        $cart->addItem($lemon, 1);
        $this->assertEquals(0.50, $cart->getPriceOf($lemon));
    }

    public function testGetPriceOf1GreenGrape()
    {
        $cart = new Cart();

        // Create a stub for the GreenGrape class.
        $greenGrape = $this->getMockBuilder('Product')
            ->setMethods(array('getType','setPrices','getPrice'))
            ->disableOriginalConstructor()
            ->getMock();

        // Configure the stub.
        $greenGrape->expects($this->any())->method('getType')->will($this->returnValue('green grape'));
        $greenGrape->expects($this->any())->method('setPrices')->will($this->returnValue([0 => 0.50, 11 => 0.45]));
        $greenGrape->expects($this->once())->method('getPrice')->will($this->returnValue(0.50));
        $greenGrape->__construct('green grape');

        $cart->addItem($greenGrape, 1);
        $this->assertEquals(0.50, $cart->getPriceOf($greenGrape));
    }

    public function testGetPriceOf11Lemons()
    {
        $cart = new Cart();
        $lemon = new Lemon();

        $cart->addItem($lemon, 11);
        $this->assertEquals(0.45, $cart->getPriceOf($lemon));
    }

    public function testGetTotalSum25Lemons()
    {
        $cart = new Cart();
        $lemon = new Lemon();

        $cart->addItem($lemon, 25);
        $this->assertEquals(11.75, $cart->getTotalSum());

    }

    public function testGetTotalSum101Tomatoes()
    {
        $cart = new Cart();
        $tomato = new Tomato();

        $cart->addItem($tomato, 101);
        $this->assertEquals(18.54, $cart->getTotalSum());

    }

    public function testGetProductTotal8Lemons()
    {
        $cart = new Cart();
        $lemon = new Lemon();

        $cart->addItem($lemon, 8);
        $this->assertEquals(4.00, $cart->getProductTotal($lemon));
    }

    public function testGetProductTotal25Tomatoes()
    {
        $cart = new Cart();
        $tomato = new Tomato();

        $cart->addItem($tomato, 25);
        $this->assertEquals(4.90, $cart->getProductTotal($tomato));
    }

    public function testGetProductTotal8LemonsAnd25Tomatoes()
    {
        $cart = new Cart();
        $lemon = new Lemon();
        $tomato = new Tomato();

        $cart->addItem($lemon, 8);
        $cart->addItem($tomato, 25);
        $this->assertEquals(4.00, $cart->getProductTotal($lemon));
        $this->assertEquals(4.90, $cart->getProductTotal($tomato));
    }
}
