<?php

/**
 * @todo Use mock objects!
 */

require_once(__DIR__.'/../src/Lemon.class.php');
require_once(__DIR__.'/../src/Tomato.class.php');

class ProductTest extends PHPUnit_Framework_TestCase {

    public function testException()
    {
        try {
            $product = new Product();
        }
        catch (Exception $expected) {
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testGetTypeLemon()
    {
        $lemon = new Lemon();
        $this->assertEquals('lemon', $lemon->getType());
        return $lemon;
    }

    public function testGetNameLemon()
    {
        $lemon = new Lemon();
        $this->assertEquals('Lemon', $lemon->getName());
        return $lemon;
    }

    /**
     * @depends testGetTypeLemon
     */
    public function testGetPrices($lemon)
    {
        $this->assertEquals([0 => 0.50, 11 => 0.45], $lemon->getPrices());
    }

    /**
     * @depends testGetTypeLemon
     */
    public function testGetPriceOf1stLemon($lemon)
    {
        $this->assertEquals(0.50, $lemon->getPrice(1));
    }

    /**
     * @depends testGetTypeLemon
     */
    public function testGetPriceOf10thLemon($lemon)
    {
        $this->assertEquals(0.50, $lemon->getPrice(10));
    }

    /**
     * @depends testGetTypeLemon
     */
    public function testGetPriceOf11thLemon($lemon)
    {
        $this->assertEquals(0.45, $lemon->getPrice(11));
    }

    public function testGetTypeTomato()
    {
        $tomato = new Tomato();
        $this->assertEquals('tomato', $tomato->getType());
        return $tomato;
    }

    /**
     * @depends testGetTypeTomato
     */
    public function testGetPriceOf1stTomato($tomato)
    {
        $this->assertEquals(0.20, $tomato->getPrice(1));
    }

    /**
     * @depends testGetTypeTomato
     */
    public function testGetPriceOf21stTomato($tomato)
    {
        $this->assertEquals(0.18, $tomato->getPrice(21));
    }

    /**
     * @depends testGetTypeTomato
     */
    public function testGetPriceOf101stTomato($tomato)
    {
        $this->assertEquals(0.14, $tomato->getPrice(101));
    }
}
