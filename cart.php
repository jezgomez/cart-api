<?php
/**
 * @author Jeremy Gomez <jeremy@agnit.io>
 * @date 27/04/2015
 */
ini_set("default_charset", 'UTF-8');

require_once(__DIR__.'/src/Lemon.class.php');
require_once(__DIR__.'/src/Tomato.class.php');
require_once(__DIR__.'/src/GreenGrape.class.php');
require_once(__DIR__.'/src/Cart.class.php');
/* start main script here */

if(php_sapi_name() !="cli"){
    throw new Exception("Only command line supported");
}

$lemon = new Lemon();
$tomato = new Tomato();
$greenGrape = new GreenGrape();

$cart = new Cart();

$cart->addItem($lemon, 8);
$cart->addItem($tomato, 25);
//$cart->addItem($greenGrape, 8);

$cart->showCartBreakdown();

?>