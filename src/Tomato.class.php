<?php

require_once('Product.class.php');

class Tomato extends Product {

    /*
     * Constructor
     *
     * @todo Empty this and set these implicitly
     *
     */
    public function __construct()
    {
        $this->setType('tomato');
        $this->setName('Tomato');
        $this->setPrices([0 => 0.20, 21 => 0.18, 101 => 0.14]);
    }
}