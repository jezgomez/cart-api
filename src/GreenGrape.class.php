<?php

require_once('Product.class.php');

class GreenGrape extends Product {

    /*
     * Constructor
     *
     * @todo Empty this and set these implicitly
     *
     */
    public function __construct()
    {
        $this->setType('greengrape');
        $this->setName('Green Grape');
        $this->setPrices([0 => 0.50, 11 => 0.45]);
    }
}