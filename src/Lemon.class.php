<?php

require_once('Product.class.php');

class Lemon extends Product {

    /*
     * Constructor
     *
     * @todo Empty this and set these implicitly
     *
     */
    public function __construct()
    {
        $this->setType('lemon');
        $this->setName('Lemon');
        $this->setPrices([0 => 0.50, 11 => 0.45]);
    }
}