<?php

class Product {

    public $type;
    public $prices = array();

    /**
     * Constructor
     */
    public function __construct($type='')
    {
        if ($type) {
            $this->setType($type);
        } else {
            throw new Exception('Invalid product type');
        }
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * setType
     *
     * @param string $type
     */
    public function setType($type='')
    {
        $this->type = $type;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setName
     *
     * @param string $name
     */
    public function setName($name='')
    {
        $this->name = $name;
    }

    /**
     * getPrices
     *
     * @return array
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * setPrices
     *
     * @param array $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * getPrice
     *
     * @return float
     */
    public function getPrice($amount = 0)
    {
        foreach($this->prices as $threshold => $price) {
            if ($threshold <= $amount) {
                $thisPrice = $price;
            }
        }
        return sprintf("%01.2f", $thisPrice);
    }
}