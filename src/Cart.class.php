<?php
require_once('CartInterface.php');

class Cart implements CartInterface {

    public $num = array();
    public $numItems = 0;
    public $productTotal = array();
    public $products = array('Lemon', 'Tomato', 'GreenGrape');
    public $total = 0.00;


    /**
     * getTotalSum
     *
     * Display a summary of the shopping cart
     * @return float
     */
    public function getTotalSum()
    {

        $lemon = new Lemon();
        $tomato = new Tomato();
        $greengrape = new GreenGrape();

        foreach ($this->num as $product => $number) {
            while ($this->num[$product]) {
                $this->total += $this->getPriceOf($$product);
                $this->num[$product]--;
            }
            $this->num[$product] = $number;
        }
        return sprintf("%01.2f", $this->total);
    }

    /**
     * getProductTotal
     *
     * Display total of particular product in the shopping cart
     * @return float
     */
    public function getProductTotal(Product $product)
    {
        if (!array_key_exists($product->getType(), $this->productTotal)) {
            $this->productTotal[$product->getType()] = 0;
        }

        $number = $this->num[$product->getType()];
        while ($this->num[$product->getType()]) {
            $this->productTotal[$product->getType()] += $this->getPriceOf($product);
            $this->num[$product->getType()]--;
        }
        $this->num[$product->getType()] = $number;
        return sprintf("%01.2f", $this->productTotal[$product->getType()]);
    }

    /**
     * addItem
     *
     * Add an item to the shopping cart
     *
     * @param Product $product Instance of the Product we're adding
     * @param int $amount The amount of $product
     *
     * @return void
     */
    public function addItem(Product $product, $amount)
    {
        if (!array_key_exists($product->getType(), $this->num)) {
            $this->num[$product->getType()] = 0;
        }
        $this->num[$product->getType()] += $amount;
    }

    /**
     * getPriceOf
     *
     * Get the price of the product depending on how many are already in the shopping cart
     *
     * @param Product $product Product The product to determine price for
     * @return float The price of $product
     */
    public function getPriceOf(Product $product)
    {
        return $product->getPrice($this->num[$product->getType()]);
    }

    /**
     * showCartBreakdown
     *
     * @todo Move 'view' out of here
     */
    public function showCartBreakdown()
    {
        foreach ($this->products as $class) {
            if (array_key_exists(strtolower($class), $this->num)) {
                $addedProducts[] = $class;
            }
        }

        foreach($addedProducts as $class) {
            $$class = new $class();
            $productTypeOutput[] = $$class->getName();
        }
        $maxProductTypeLen = max(array_map('strlen', $productTypeOutput));

        foreach ($addedProducts as $class) {
            echo $this->num[$$class->getType()] . chr(9) . str_pad(ucwords($$class->getName()), $maxProductTypeLen) . chr(9) . '£'.  $this->getProductTotal($$class) . PHP_EOL;
        }
        echo '-----------------'. PHP_EOL;
        echo 'Total:' . str_pad(chr(9), $maxProductTypeLen/3, chr(9)) . '£' . $this->getTotalSum() . PHP_EOL;
    }

}